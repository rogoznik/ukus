#!/bin/bash

# notification.sh

# Copyright (C) 2019  Nikita Rogoz <nikrogoz@yandex.ru>

# This file is part of UKUS.

#     UKUS is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     UKUS is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with UKUS.  If not, see <https://www.gnu.org/licenses/>.

p=$(readlink -e "$0")
d=$(dirname "$p")

source "$d/functions.sh"

init

LTS=(4.9 4.14 4.19 5.4)

while [ 1=1 ]
do
    curl "$serverUrl" > "${dir}/1.html"

    isKLTS=false

    IFS='.' read -ra arrCurrKernel <<< "$currKernel"

    arrKernel=($(cat "${dir}/1.html" |\
        sed -e "/href=\"v${arrCurrKernel[0]}\.${arrCurrKernel[1]}\./b" \
        -e d |\
        sed -e "s/^<t.*href=\"//" |\
        sed -e "s/\/\">.*>$//" |\
        sort -Vr))

    IFS='.' read -ra arrK <<< $(echo ${arrKernel[0]} | cut -c 2-)

    for k in ${LTS[@]}
    do
        if [[ "${k}" == "${arrCurrKernel[0]}.${arrCurrKernel[1]}" ]]
        then
            isKLTS=true
        fi
    done

    if [[ "${isKLTS}" == "true" ]]
    then
        if (( ${arrK[2]} > ${arrCurrKernel[2]} ))
        then
            zenity --notification \
                    --window-icon="info" \
                    --text="${textNotifycation}${arrKernel[0]}"
        fi
    fi

    sleep 3600
done