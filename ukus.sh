#!/bin/bash

# ukus.sh

# Copyright (C) 2019  Nikita Rogoz <nikrogoz@yandex.ru>

# This file is part of UKUS.

#     UKUS is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     UKUS is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with UKUS.  If not, see <https://www.gnu.org/licenses/>.

p=$(readlink -e "$0")
d=$(dirname "$p")

source "$d/functions.sh"

if [[ -n "$1" ]]
then
    if [[ "$1" = "--set-language" ]]
    then
        selectLenguage
        exit 0
    fi
fi

init

if [[ $initiScript = false ]]
then
    exit 0
fi

(
    curl "$serverUrl" > "${dir}/1.html"
    echo "100"
) | \
    zenity --progress \
        --pulsate \
        --width=400 \
        --title="$title" \
        --text="$dataAcquisition" \
        --percentage=0 \
        --auto-close \
        --no-cancel

if [[ -f "${dir}/1.html" ]]
then
    if [[ ! -s "${dir}/1.html" ]]
    then
        zenity --error --width=400 --title="$title" --text="$unableToConnectToServer"
        exit 1
    fi
else
    zenity --error --width=400 --title="$title" --text="$failedToCreateFile"
    exit 1
fi

v=$(cat "${dir}/1.html" |\
        sed -e "/href=\"v4\.4\./b" \
        -e "/href=\"v4\.9\./b" \
        -e "/href=\"v4\.14\./b" \
        -e "/href=\"v4\.19\./b" \
        -e "/href=\"v5\.4\./b" \
        -e d |\
        sed -e "s/^<t.*href=\"//" |\
        sed -e "s/\/\">.*>$//" |\
        sort -Vr |\
        zenity --list \
            --width=400 \
            --height=600 \
            --title="$title" \
            --text="$selectTheKernelToInstall" \
            --column="$columnVersion"
)

if [[ $? -eq "0" ]]
then
    if [[ "$v" ]]
    then
        installing "$v"
    else
        zenity --error --width=400 --title="$title" \
            --text="$notSelectedKernel"
        exit 1
    fi
fi

exit 0