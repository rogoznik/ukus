## Ubuntu Kernel Update Script (UKUS)

This script is to install the latest mainline LTS Linux kernel into Ubuntu-based distributions

For the script to work, you must install `zenity`

    sudo apt install zenity

## Features

* Fetches list of kernels LTS from [kernel.ubuntu.com](https://kernel.ubuntu.com/~kernel-ppa/mainline/)
* Downloads and installs packages automatically
* Log setup in the `/var/log/ukus/install.log` file
* Setting the interface language
* Kernel update notification

## Installation

    wget https://gitlab.com/rogoznik/ukus/-/archive/master/ukus-master.tar.gz
    sudo tar -xvzf ukus-master.tar.gz -C /opt
    sudo chmod a+x /opt/ukus-master/ukus.sh
    sudo chmod a+x /opt/ukus-master/notification.sh
    sudo ln -s /opt/ukus-master/ukus.sh /usr/bin/ukus
    copy /opt/ukus-master/ukus-notify.desktop /home/$USER/.config/autostart/ukus-notify.desktop

## Setting the interface language

    ukus --set-language

The setting will be saved to a file `$HOME/.config/ukus/config.ini`