# functions.sh

# Copyright (C) 2019  Nikita Rogoz <nikrogoz@yandex.ru>

# This file is part of UKUS.

#     UKUS is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     UKUS is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with UKUS.  If not, see <https://www.gnu.org/licenses/>.

function init {
    serverUrl="https://kernel.ubuntu.com/~kernel-ppa/mainline/"
    currKernel=$(uname -r | sed "s/-.*//")
    title="UKUS - current kernel ${currKernel}"
    dir="/tmp/ukus"
    dDir="${dir}/downloads"
    configDir="${HOME}/.config/ukus"
    configFile="${configDir}/config.ini"
    lngDir="${d}/lng"

    noConfig=true
    initiScript=false

    if [[ ! -d "$dir" ]]
    then
        mkdir "$dir"
    fi

    if [[ -f "$configFile" ]]
    then
        if [[ -s "$configFile" ]]
        then
            noConfig=false
        fi
    fi
    readConfig
}

function readConfig {
    language
}

function language {
    if [[ $noConfig == false ]]
    then
        lng=$(sed -n '/language/{n;p;}' $configFile)
        initiScript=true
    else
        selectLenguage
    fi
    
    if [[ $lng != "" ]]
    then
        source "${lngDir}/${lng}.sh"
    fi
    
}

function installing {    
    if [[ ! -d "$dDir" ]]
    then
        mkdir "$dDir"
    fi

    local version="$1"

    local url="${serverUrl}${version}/";

    curl "$url" > "${dir}/2.html"

    local b=$(uname -i | sed "s/_/-/")

    if [ "$b" = "x86-64" ]
    then
        local sys="amd64"
    else
        local sys="i386"
    fi

    files=()

    local file1=$(cat "${dir}/2.html" | \
        sed -n "/<tr.*linux-headers-.*all\.deb/p" | \
        sed -e "s/^<tr.*deb\">//" | \
        sed -e "s/<\/a.*$//"
    )
    local dUrl1="$url$file1"
    files+=( $(echo "${dDir}/${version}/${file1}") )

    local file2=$(cat "${dir}/2.html" | \
        sed -n "/<tr.*linux-headers-.*-generic.*${sys}\.deb/p" | \
        sed -e "s/^<tr.*deb\">//" | \
        sed -e "s/<\/a.*$//"
    )
    local dUrl2="$url$file2"
    files+=( $(echo "${dDir}/${version}/${file2}") )

    local file3=$(cat "${dir}/2.html" | \
        sed -n "/<tr.*linux-modules-.*-generic.*${sys}\.deb/p" | \
        sed -e "s/^<tr.*deb\">//" | \
        sed -e "s/<\/a.*$//"
    )
    local dUrl3="$url$file3"
    if [[ "$file3" ]]
    then
        files+=( $(echo "${dDir}/${version}/${file3}") )
    fi
    

    local file4=$(cat "${dir}/2".html | \
        sed -n "/<tr.*linux-image-unsigned-.*-generic.*${sys}\.deb/p" | \
        sed -e "s/^<tr.*deb\">//" | \
        sed -e "s/<\/a.*$//"
    )
    if [[ ! "$file4" ]]
    then
        local file4=$(cat "${dir}/2.html" | \
        sed -n "/<tr.*linux-image-.*-generic.*${sys}\.deb/p" | \
        sed -e "s/^<tr.*deb\">//" | \
        sed -e "s/<\/a.*$//"
    )
    fi
    local dUrl4="$url$file4"
    files+=( $(echo "${dDir}/${version}/${file4}") )

    if [ -z "$file1" -a -z "$file2" -a -z "$file3" -a -z "$file4" ]
    then
        zenity --error --width=400 \
            --title="$title" \
            --text="$noFilesToDownloads"
        exit 0
    fi

    if [[ ! -d "$dDir/$version" ]]
    then 
        mkdir "$dDir/$version"
    fi

    (
        wget -nc "$dUrl1" -O "${dDir}/${version}/${file1}"
        wget -nc "$dUrl2" -O "${dDir}/${version}/${file2}"
        if [[ "$file3" ]]
        then
            wget -nc "$dUrl3" -O "${dDir}/${version}/${file3}"
        fi
        wget -nc "$dUrl4" -O "${dDir}/${version}/${file4}"
        echo "100"
    ) | \
        zenity --progress \
            --pulsate \
            --percentage=0 \
            --width=400 \
            --title="$title" \
            --text="$downloading" \
            --auto-close \
            --no-cancel
    
    local inF="${dir}install.sh"
    if [[ -f "$inF" ]]
    then
        rm -f "$inF"
        touch "$inF"
    else
        touch "$inF"
    fi

    echo "#!/bin/bash" >> "$inF"
    echo "exec &> /var/log/ukus/install.log" >> "$inF"
    echo "if [ ! -d \"/var/log/ukus\" ]; then mkdir "/var/log/ukus"; fi" >> "$inF"
    echo "if [ -f \"/var/log/ukus/install.log\" ]; then echo -n > "/var/log/ukus/install.log"; fi" >> "$inF"

    for f in ${files[@]}
    do
        echo "dpkg -i ${f}" >> "$inF"
    done

    echo "update-grub" >> "$inF"
    chmod a+x "$inF"
    
    (
        pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY "$inF"
        echo "100"
    ) | \
        zenity --progress \
            --pulsate \
            --percentage=0 \
            --width=400 \
            --title="$title" \
            --text="$installing" \
            --auto-close \
            --no-cancel
    
    zenity --info \
        --width=400 \
        --title="$title" \
        --text="$installationIsComplite"
}

function setLanguage {
    if [[ -f "$configFile" ]]
    then
        local s1=$(grep -n -A1 "language" $configFile | sed -e "/rus/b" -e "/eng/b" -e d)
        local s2=${s1:(-3)}
        local indx=$(expr index "$s1" "$s2")
        local l=$(echo ${#s1})
        local diff=$(( $l-$indx ))
        local num=${s1:0:$(( $diff - 1 ))}
        sed -i -e "${num} i${1}" "$configFile"
        sed -i -e "$(( num + 1 )) d" "$configFile"
    else
        if [ ! -d "$configDir" ]
        then
            mkdir "$configDir"
        fi
        echo "[language]" >> $configFile
        echo "$1" >> $configFile
    fi
}

function selectLenguage {
    lng=$(zenity --list --radiolist \
        --title="Language" \
        --text="Choose language:" \
        --column="" --column="Language" \
        FALSE rus TRUE eng
    )
    
    if [[ $? -eq "0" ]]
    then
        setLanguage "$lng"
        initiScript=true
        lng="$lng"
    else
        initiScript=false
        lng=""
    fi
}